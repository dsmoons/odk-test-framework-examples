package your.domain.autotests.pages;

import io.bitbucket.dsmoons.framework.odk.BaseHelper;
import io.bitbucket.dsmoons.framework.odk.PageHelper;
import your.domain.autotests.pages.hq.Contacts;
import your.domain.autotests.pages.hq.Default;
import your.domain.autotests.pages.hq.Post;

@SuppressWarnings("unused")
public class Pages extends PageHelper {
    public Default hqDefault = getPage(Default.class);
    public Contacts contacts = getPage(Contacts.class);
    public Post post = getPage(Post.class);

    public Pages(BaseHelper base) {
        super(base);
    }
}
