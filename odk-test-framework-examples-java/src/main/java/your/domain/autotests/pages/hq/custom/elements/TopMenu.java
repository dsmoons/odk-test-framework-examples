package your.domain.autotests.pages.hq.custom.elements;

import io.bitbucket.dsmoons.framework.odk.AllureHelper;
import io.bitbucket.dsmoons.framework.odk.BaseHelper;
import io.bitbucket.dsmoons.framework.odk.PageBase;
import io.bitbucket.dsmoons.framework.odk.wrappers.Element;
import your.domain.autotests.pages.hq.Contacts;

@SuppressWarnings("unused")
public class TopMenu extends PageBase<TopMenu> {

    private final Element menu = getElementById("top-menu", "Верхнее меню");

    private final Element blog = getElementByXpath(menu, "//*[text()='Блог']", "Ссылка на блог");
    private final Element odk = getElementByXpath(menu, "//*[text()='ODk Test Framework']", "Ссылка 'ODk Test Framework'");
    private final Element odf = getElementByXpath(menu, "//*[text()='ODf Test Framework']", "Ссылка 'ODf Test Framework'");
    private final Element contacts = getElementByXpath(menu, "//*[text()='Контакты']", "Ссылка на контакты");

    public TopMenu(BaseHelper baseHelper) {
        super(baseHelper);
    }

    public Contacts goToContactsPage() {
        return AllureHelper.runStep("Переход на страницу контактов", () -> {
            contacts.click().waitFor(Element::notExists);
            return getPage(Contacts.class);
        });
    }
}
