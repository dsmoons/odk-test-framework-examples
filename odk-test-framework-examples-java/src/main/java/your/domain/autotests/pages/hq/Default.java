package your.domain.autotests.pages.hq;

import io.bitbucket.dsmoons.framework.odk.AllureHelper;
import io.bitbucket.dsmoons.framework.odk.BaseHelper;
import io.bitbucket.dsmoons.framework.odk.PropertiesHelper;
import io.bitbucket.dsmoons.framework.odk.wrappers.Element;
import your.domain.autotests.pages.hq.custom.elements.FirstPost;

import java.util.Objects;

@SuppressWarnings("UnusedReturnValue")
public class Default extends CommonElements<Default> {

    public FirstPost firstPost = getPage(FirstPost.class);

    private final Element firstPostLink = getElementByXpath("//div/h2/a", "Ссылка на пост");
    private final Element firstPostReadLink = getElementByXpath("//span[@class='next']/a", "Ссылка 'Читать далее'");

    public Default(BaseHelper baseHelper) {
        super(baseHelper, PropertiesHelper.getProperty("link.hq"));
    }

    public Default firstPostReadLinkActions() {
        AllureHelper.runStep("Действия с элементом " + firstPostReadLink,
                () -> firstPostReadLink
                        .waitForExistsAnd(element -> !Objects.equals("", element.getText()))
                        .assertThat(Element::exists, "Элемент не найден")
                        .assertThat(Element::isEnabled, "Элемент недоступен")
                        .takeScreenshot());
        return this;
    }

    public Default firstPostLinkActions() {
        AllureHelper.runStep("Действия с элементом " + firstPostLink,
                () -> firstPostLink
                        .assertThat(Element::exists, "Элемент не найден")
                        .toList()
                        .forEach(Element::takeScreenshot));
        return this;
    }
}
