package your.domain.autotests.pages.hq.custom.elements;

import io.bitbucket.dsmoons.framework.odk.AllureHelper;
import io.bitbucket.dsmoons.framework.odk.Assert;
import io.bitbucket.dsmoons.framework.odk.BaseHelper;
import io.bitbucket.dsmoons.framework.odk.PageBase;
import io.bitbucket.dsmoons.framework.odk.wrappers.Element;
import your.domain.autotests.pages.hq.Post;

public class FirstPost extends PageBase<FirstPost> {

    private final Element post = getElementByXpath("//div[@class='post'][1]", "Первый пост");

    private final Element header = getElementByXpath(post, "//h2/a", "Заголовок-ссылка");
    private final Element firstParagraph = getElementByXpath(post, "//p", "Первый параграф");
    private final Element date = getElementByClassName(post, "data", "Дата");
    private final Element next = getElementByXpath(post, "//span[@class='next']/a", "Ссылка 'Читать далее'");
    private final Element tags = getElementByClassName(post, "tag", "Теги");

    public FirstPost(BaseHelper baseHelper) {
        super(baseHelper);
    }

    public FirstPost compareHeaderAndNextLink() {
        AllureHelper.runStep("Сравнение ссылок на пост", () -> {
            var headerLink = header.waitForExists().getDomAttribute("href");
            var nextLink = next.getDomAttribute("href");
            Assert.areEqualAndAccumulate(headerLink, nextLink, "Ссылки на пост", "Не совпадают ссылки на пост");
        });
        return this;
    }

    public String getFirstParagraphText() {
        return AllureHelper.runStep("Получение текста первого параграфа первого поста на главной странице",
                firstParagraph::getText);
    }

    public String getDate() {
        return AllureHelper.runStep("Получение даты первого поста на главной странице", date::getText);
    }

    public String getTags() {
        return AllureHelper.runStep("Получение тегов первого поста на главной странице", tags::getText);
    }

    public String getHeaderText() {
        return AllureHelper.runStep("Получение текста ссылки-заголовка первого поста на главной странице",
                header::getText);
    }

    @SuppressWarnings("UnusedReturnValue")
    public Post goToPostPage() {
        return AllureHelper.runStep("Переход на страницу первого поста", () -> {
            next.click().waitFor(Element::notExists);
            return getPage(Post.class);
        });
    }
}
