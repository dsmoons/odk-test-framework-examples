package your.domain.autotests.pages.hq;

import io.bitbucket.dsmoons.framework.odk.AllureHelper;
import io.bitbucket.dsmoons.framework.odk.BaseHelper;
import io.bitbucket.dsmoons.framework.odk.wrappers.Element;

import java.io.File;

public class Contacts extends CommonElements<Contacts> {

    private final Element nameElement = getElementById("full-name", "Поле 'Имя'");

    public Contacts(BaseHelper baseHelper) {
        super(baseHelper);
    }

    public Contacts setName(String name) {
        AllureHelper.runStep("Заполнение имени", () -> nameElement.waitForExists().sendKeys(name));
        return this;
    }

    public File getNameScreenshot() {
        return nameElement.takeScreenshot();
    }
}
