package your.domain.autotests.pages.hq;

import io.bitbucket.dsmoons.framework.odk.BaseHelper;
import io.bitbucket.dsmoons.framework.odk.PageBase;
import io.bitbucket.dsmoons.framework.odk.wrappers.Element;
import your.domain.autotests.pages.hq.custom.elements.TopMenu;

@SuppressWarnings("unused")
public class CommonElements<T extends PageBase<T>> extends PageBase<T> {

    public TopMenu topMenu = getPage(TopMenu.class);

    public CommonElements(BaseHelper baseHelper, String link) {
        super(baseHelper, link);
    }

    public CommonElements(BaseHelper baseHelper) {
        super(baseHelper);
    }

    protected Element getButtonByText(String text) {
        return getElementByXpath("//button[text='" + text + "']", "Кнопка '" + text + "'");
    }

    protected Element getElementByAttribute(String attribute, String value) {
        return getElementByXpath("//*[@" + attribute + "='" + value + "']",
                "Элемент с атрибутом '" + attribute + "' равным '" + value + "'");
    }

    // Примеры классов, облегчающих создание элементов
    public class Button extends Element {

        public Button(String text) {
            super(CommonElements.this.getElementByXpath("//button[text='" + text + "']", "Кнопка '" + text + "'"));
        }
    }

    public class Checkbox extends Element {

        public Checkbox(String label) {
            super(CommonElements.this.getElementByXpath("//label[text='" + label + "']/input[@type='checkbox']",
                    "Флажок '" + label + "'"));
        }

        public Checkbox check() {
            if (!super.getSelect().isSelected()) {
                click();
            }
            return this;
        }

        public Checkbox uncheck() {
            if (super.getSelect().isSelected()) {
                click();
            }
            return this;
        }
    }
}
