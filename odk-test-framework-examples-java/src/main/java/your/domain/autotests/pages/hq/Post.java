package your.domain.autotests.pages.hq;

import io.bitbucket.dsmoons.framework.odk.AllureHelper;
import io.bitbucket.dsmoons.framework.odk.BaseHelper;
import io.bitbucket.dsmoons.framework.odk.wrappers.Element;

public class Post extends CommonElements<Post> {

    private final Element postHeader = getElementByXpath("//div[@class='content']/h2", "Заголовок поста");
    private final Element firstParagraphText = getElementByClassName("first", "Первый параграф");
    private final Element date = getElementByClassName("data", "Дата");
    private final Element tags = getElementByClassName("tag", "Теги");

    public Post(BaseHelper baseHelper) {
        super(baseHelper);
    }

    public String getPostHeaderText() {
        return AllureHelper.runStep("Получение заголовка поста", postHeader::getText);
    }

    public String getFirstParagraphText() {
        return AllureHelper.runStep("Получение текста первого параграфа", firstParagraphText::getText);
    }

    public String getDate() {
        return AllureHelper.runStep("Получение даты поста", date::getText);
    }

    public String getTags() {
        return AllureHelper.runStep("Получение тегов поста", tags::getText);
    }
}
