package your.domain.autotests;

import io.bitbucket.dsmoons.framework.odk.BaseHelper;
import io.bitbucket.dsmoons.framework.odk.HandleException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import your.domain.autotests.pages.Pages;

@HandleException
@Execution(ExecutionMode.SAME_THREAD)
public class SuiteBaseUi {

    Pages pages;
    Pages pagesClone;

    private BaseHelper base;

    @BeforeEach
    void setUp(TestInfo testInfo) {
        base = new BaseHelper.Builder(testInfo)
                .setPropertiesFile("profile.properties")
                .setEnvironmentPropertiesFile("environment.properties")
                .setWebDriverCallable(WebDriverHelper::getDriver)
                .setWebDriverLogsAddToReport()
                .build();

        pages = new Pages(base);

        base.start();

        /* Контейнер страниц, созданный с помощью клона объекта базового класса (BaseHelper).
         * При первом обращении к методам его страниц будет создан драйвер, переданный в метод setDriverCallable,
         * и открыт соответствующий браузер.
         * Этот механизм нужен для кейсов, в которых предполагается работа в двух и более (в зависимости от количества
         * контейнеров страниц) разных браузерах.
         * Вызывать метод stop у клона нет необходимости. Закрытие браузеров из клонов и добавление их логов в отчет
         * произойдет при вызове метода stop у оригинального объекта BaseHelper.
         * Вызов метода stop у клона будет проигнорирован
         */
        pagesClone = new Pages(base.clone().setDriverCallable(() -> WebDriverHelper.getDriver("Edge")));
    }

    @AfterEach
    void tearDown() {
        base.stop();
    }
}
