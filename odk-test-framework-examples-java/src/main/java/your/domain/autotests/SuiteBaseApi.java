package your.domain.autotests;

import io.bitbucket.dsmoons.framework.odk.BaseHelper;
import io.bitbucket.dsmoons.framework.odk.HandleException;
import io.bitbucket.dsmoons.framework.odk.PropertiesHelper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import your.domain.autotests.services.RestPetstoreClient;
import your.domain.autotests.services.SoapNumberConversionServiceClient;

@HandleException
@Execution(ExecutionMode.CONCURRENT)
public class SuiteBaseApi {

    SoapNumberConversionServiceClient soapNumberConversionServiceClient;
    RestPetstoreClient restPetstoreClient;
    Database database;

    BaseHelper.ThreadUtil threadUtil;

    private BaseHelper base;

    @BeforeEach
    void setUp(TestInfo testInfo) {
        base = new BaseHelper.Builder(testInfo)
                .setPropertiesFile("profile.local.properties", "profile.properties")
                .setEnvironmentPropertiesFile("environment.properties")
                .build();

        threadUtil = base.getThreadUtil();

        soapNumberConversionServiceClient = new SoapNumberConversionServiceClient(PropertiesHelper.getProperty("number.conversion.service.soap.wsdl.host"));
        restPetstoreClient = new RestPetstoreClient(PropertiesHelper.getProperty("petstore.rest.host"));

        database = new Database();

        base.start();
    }

    @AfterEach
    void tearDown() {
        restPetstoreClient.close();
        base.stop();
    }
}
