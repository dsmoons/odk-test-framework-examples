package your.domain.autotests.services;

import io.bitbucket.dsmoons.framework.odk.Assert;
import io.bitbucket.dsmoons.framework.odk.ContentHelper;
import io.bitbucket.dsmoons.framework.odk.api.RestException;
import io.bitbucket.dsmoons.framework.odk.api.RestResponse;
import io.bitbucket.dsmoons.framework.odk.api.RestServiceClientHelper;
import jakarta.ws.rs.core.MediaType;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.apache.hc.core5.http.message.BasicClassicHttpRequest;
import org.apache.hc.core5.http.message.BasicHeader;
import your.domain.autotests.services.rest.petstore.Order;
import your.domain.autotests.services.rest.petstore.Pet;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@SuppressWarnings("unused")
public class RestPetstoreClient extends RestServiceClientHelper {

    public RestPetstoreClient(String host) {
        super(host, new BasicHeader("Content-Type", MediaType.APPLICATION_JSON));
    }

    public RestResponse<Pet[]> findByStatus(Pet.StatusEnum status) {
        return sendGetRequest(Pet[].class, "v2/pet/findByStatus?status=" + status);
    }

    public RestResponse<Pet[]> findByStatus(Pet.StatusEnum status, Boolean needThrow) throws RestException {
        return sendGetRequest(needThrow, Pet[].class, "v2/pet/findByStatus?status=" + status);
    }

    public RestResponse<Order> order(Order request) {
        return sendPostRequest(Order.class, "v2/store/order", request);
    }

    public RestResponse<Order> customOrder(Order request) {
        var requestBase = new BasicClassicHttpRequest("POST", "");
        try {
            requestBase.setEntity(new StringEntity(ContentHelper.serializeToJsonString(request), StandardCharsets.UTF_8));
        } catch (IOException exception) {
            Assert.handleException(exception);
        }
        return sendCustomRequest(Order.class, "v2/store/order", requestBase);
    }

    public RestResponse<Order> order(Order request, Boolean needThrow) throws RestException {
        return sendPostRequest(needThrow, Order.class, "v2/store/order", request);
    }
}
