package your.domain.autotests.services;

import io.bitbucket.dsmoons.framework.odk.api.SoapResponse;
import io.bitbucket.dsmoons.framework.odk.api.SoapServiceClientHelper;
import your.domain.autotests.services.soap.numberconversion.NumberConversion;

import java.math.BigInteger;
import java.net.URL;

public class SoapNumberConversionServiceClient extends SoapServiceClientHelper {
    public SoapNumberConversionServiceClient(String serviceLink) {
        super(() -> new NumberConversion(new URL(serviceLink)).getNumberConversionSoap12());
    }

    public SoapResponse<String> numberToWords(BigInteger number) {
        return getResponse("numberToWords", number);
    }
}
