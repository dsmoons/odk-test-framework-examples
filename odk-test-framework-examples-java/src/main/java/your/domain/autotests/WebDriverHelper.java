package your.domain.autotests;

import io.bitbucket.dsmoons.framework.odk.Assert;
import io.bitbucket.dsmoons.framework.odk.PropertiesHelper;
import io.bitbucket.dsmoons.framework.odk.SettingsHelper;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class WebDriverHelper {

    public static WebDriver getDriver() {
        return getDriver(PropertiesHelper.getProperty("browser"));
    }

    public static WebDriver getDriver(String browser) {
        return switch (browser) {
            case "SelenoidChrome" -> getSelenoidChrome();
            case "SelenoidFirefox" -> getSelenoidFirefox();
            case "EdgeHeadless" -> getEdgeDriver(true);
            case "Edge" -> getEdgeDriver(false);
            case "FirefoxHeadless" -> getFirefoxDriver(true);
            case "Firefox" -> getFirefoxDriver(false);
            case "ChromeHeadless" -> getChromeDriver(true);
            default -> getChromeDriver(false);
        };
    }

    @SuppressWarnings({"unused", "ConstantConditions"})
    public static RemoteWebDriver getWinAppDriver() {
        URL url = null;
        try {
            url = new URI(PropertiesHelper.getProperty("winappdriver.host")).toURL();
        } catch (Exception exception) {
            Assert.handleException(exception, "Ошибка создания ссылки на драйвер");
        }
        return new RemoteWebDriver(url, new MutableCapabilities(Map.of("app", "Root")));
    }

    private static ChromeDriver getChromeDriver(Boolean headless) {
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver(getChromeOptions(headless));
    }

    private static FirefoxDriver getFirefoxDriver(Boolean headless) {
        System.setProperty(GeckoDriverService.GECKO_DRIVER_LOG_PROPERTY, "/dev/null");
        WebDriverManager.firefoxdriver().setup();
        return new FirefoxDriver(getFirefoxOptions(headless));
    }

    private static EdgeDriver getEdgeDriver(Boolean headless) {
        WebDriverManager.edgedriver().setup();
        var options = new EdgeOptions();
        options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
        options.setEnableDownloads(true);
        options.addArguments("--no-sandbox", "--remote-allow-origins=*", "--disable-gpu", "--disable-dev-shm-usafe");
        if (headless) {
            options.addArguments("--headless");
        }
        options.setAcceptInsecureCerts(true);
        var prefs = new HashMap<String, Object>(
                Map.of("download.default_directory", new File("target//downloads").getAbsolutePath()));
        options.setExperimentalOption("prefs", prefs);
        return new EdgeDriver(options);
    }

    private static RemoteWebDriver getSelenoidChrome() {
        return getSelenoidRemoteWebDriver(getChromeOptions(false));
    }

    private static RemoteWebDriver getSelenoidFirefox() {
        return getSelenoidRemoteWebDriver(getFirefoxOptions(false));
    }

    private static ChromeOptions getChromeOptions(Boolean headless) {
        var options = new ChromeOptions();
        //options.setCapability("safebrowsing.enabled", "false");
        options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
        options.setEnableDownloads(true);
        options.addArguments("--no-sandbox", "--remote-allow-origins=*", "--disable-gpu", "--disable-dev-shm-usafe");
        if (headless) {
            options.addArguments("--headless");
        }
        options.setAcceptInsecureCerts(true);
        options.setCapability("se:downloadsEnabled", true);
        var prefs = new HashMap<String, Object>(
                Map.of("download.default_directory", new File("target//downloads1").getAbsolutePath()));
        options.setExperimentalOption("prefs", prefs);
        return options;
    }

    private static FirefoxOptions getFirefoxOptions(Boolean headless) {
        var options = new FirefoxOptions();
        var mimeTypes = "text/csv,application/msexcel,application/x-msexcel,application/excel,application/x-excel," +
                "application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword," +
                "application/xml,application/pdf,application/octet-stream";
        options.addPreference("browser.helperApps.neverAsk.openFile", mimeTypes);
        options.addPreference("browser.helperApps.neverAsk.saveToDisk", mimeTypes);
        options.addPreference("browser.helperApps.alwaysAsk.force", false);
        options.addPreference("browser.download.folderList", 2);
        options.addPreference("browser.download.manager.showWhenStarting", false);
        options.addPreference("browser.download.dir", new File("target//downloads").getAbsolutePath());
        options.addPreference("browser.download.manager.alertOnEXEOpen", false);
        options.addPreference("browser.download.manager.focusWhenStarting", false);
        options.addPreference("browser.download.manager.useWindow", false);
        options.addPreference("browser.download.manager.showAlertOnComplete", false);
        options.addPreference("browser.download.manager.closeWhenDone", false);
        options.addPreference("browser.startup.homepage_override.mstone", "ignore");
        options.addPreference("startup.homepage_welcome_url.additional", "about:blank");
        options.addPreference("plugin.scan.Acrobat", "99.0");
        options.addPreference("plugin.scan.plid.all", false);
        options.addPreference("plugin.disable_full_page_plugin_for_types", mimeTypes);
        options.addPreference("pdfjs.disabled", true);
        options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
        if (headless) {
            options.addArguments("--headless");
        }
        return options;
    }

    private static RemoteWebDriver getSelenoidRemoteWebDriver(MutableCapabilities capabilities) {
        capabilities.setCapability("selenoid:options",
                Map.of("enableVNC", true, "enableVideo", false, "enableLog", false));
        URL url = null;
        try {
            url = new URI(PropertiesHelper.getProperty("selenoid.host")).toURL();
        } catch (Exception exception) {
            Assert.handleException(exception, "Ошибка создания ссылки на драйвер");
        }
        var driver = new RemoteWebDriver(url, capabilities);
        driver.setFileDetector(new LocalFileDetector());
        return driver;
    }
}
