package your.domain.autotests;

import io.bitbucket.dsmoons.framework.odk.DbHelper;
import your.domain.autotests.db.chinook.Employees;

import java.util.List;
import java.util.Map;

import static io.bitbucket.dsmoons.odk.sql.query.builder.PredicateObject.field;
import static io.bitbucket.dsmoons.odk.sql.query.builder.QueryObject.delete;
import static io.bitbucket.dsmoons.odk.sql.query.builder.QueryObject.select;

@SuppressWarnings("CommentedOutCode")
public class Database {

    private final DbHelper sqlite;

    public Database() {
        //var db = new DbHelper("jdbcUrl").setDriverClass(org.sqlite.JDBC.class);
        //var db = new DbHelper("jdbcUrl").setDriverClass(org.sqlite.JDBC.class).setCountOfEntitiesForLogging(200);
        //var db = new DbHelper("jdbcUrl", "login", "password");
        //var db = new DbHelper("jdbcUrl", "login", "password").setDriverClass(org.sqlite.JDBC.class);
        //var db = new DbHelper("jdbcUrl", "login", "password").setDriverClass(org.sqlite.JDBC.class).setCountOfEntitiesForLogging(200);

        sqlite = new DbHelper("jdbc:sqlite:src/main/resources/db/chinook.db");

        //sqlite.setCatalog("catalogName").setSchema("schemaName");
    }

    public Integer getEmployeesCount() {
        return sqlite.executeQuery.forListAndGetFirst(select("count(*)").from("employees").build(), Integer.class,
                "Получение количества записей в таблице employees");
    }

    public List<Employees> getEmployeesListByCity(String city) {
        var query = select().from(Employees.class).where(field("City").equalTo(city)).build();
        return sqlite.executeQuery.forList(query, Employees.class,
                "Получение записей из таблицы employees по названию города");
    }

    public Employees getEmployeeByName(String firstName, String lastName) {
        var query = select().from(Employees.class).where(Map.of("FirstName", firstName, "LastName", lastName));
        return sqlite.executeQuery.forListAndGetFirst(query.build(), Employees.class,
                "Получение записи из таблицы employees по имени");
    }

    public Integer deleteEmployee(Employees employee) {
        var query = delete().from(Employees.class)
                .where(field("FirstName").equalTo(employee.getFirstName())).build();
        return sqlite.executeQuery.withoutResponse(query, "Удаление записи с FirstName = " + employee.getFirstName());
    }

    public Integer insertEmployee(Employees employee) {
        return sqlite.insert.entity("employees", employee, "Новый employee");
    }

    public Integer insertEmployees(List<Employees> employees) {
        return sqlite.insert.entities("employees", employees, "Новые employee");
    }
}
