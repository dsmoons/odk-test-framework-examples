package your.domain.autotests;

import io.bitbucket.dsmoons.framework.odk.Assert;
import io.bitbucket.dsmoons.framework.odk.ContentHelper;
import io.bitbucket.dsmoons.framework.odk.Logger;
import io.bitbucket.dsmoons.framework.odk.Wait;
import io.qameta.allure.Epic;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import your.domain.autotests.db.chinook.Employees;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;

@DisplayName("Примеры тестов, показывающие работу хелперов")
@Epic("Примеры тестов, показывающие работу хелперов")
class HelpersExamplesTests extends SuiteBaseApi {

    @DisplayName("Пример доступа к базе данных")
    @Test
    void testDatabase() {
        var count = database.getEmployeesCount();
        Assert.areEqualAndAccumulate(8, count, "Количество записей",
                "Неправильное количество записей");

        var employees = database.getEmployeesListByCity("Calgary");
        Assert.areEqualAndAccumulate(5, employees.size(), "Количество записей по городу \"Calgary\"",
                "Неправильное количество записей");

        var employee = database.getEmployeeByName("Robert", "King");
        Assert.areEqual("IT Staff", employee.getTitle(), "Title", "Неправильный Title");

        employee.setFirstName(employee.getFirstName() + ContentHelper.getTimestamp());
        employee.setLastName(employee.getLastName() + ContentHelper.getTimestamp());
        employee.setEmployeeId(null);
        var insertEmployee = database.insertEmployee(employee);
        Assert.areEqualAndAccumulate(1, insertEmployee, "Количество строк, затронутое вставкой в БД",
                "Неправильное количество строк, затронутое вставкой в БД");

        deleteEmployee(employee);

        var newEmployees = new EasyRandom(new EasyRandomParameters().seed(Long.parseLong(ContentHelper.getTimestamp())))
                .objects(Employees.class, 2).toList();
        newEmployees.forEach(e -> e.setEmployeeId(null));

        var insertEmployees = database.insertEmployees(newEmployees);
        Assert.areEqualAndAccumulate(newEmployees.size(), insertEmployees, "Количество строк, затронутое вставкой в БД",
                "Неправильное количество строк, затронутое вставкой в БД");

        newEmployees.forEach(this::deleteEmployee);
    }

    @DisplayName("Тест с ожиданием выполнения условия")
    @Test
    void testWait() {
        // Получение данных через ожидание выполнения условия
        var timestamp = Wait
                .byDuration(
                        // Время на ожидание
                        Duration.ofSeconds(10),
                        // Интервал, через который повторяется проверка условия
                        Duration.ofMillis(200)
                )
                // Описание ожидания
                .description("Timestamp заканчивается на 4")
                // Нужно ли завершить тест и отметить его непройденным, если условие не выполнено;
                // сообщение об ошибке
                .needFailWithMessage("Не удалось получить нужное значение")
                // Метод ожидания
                .waitFor(
                        ContentHelper::getTimestamp, // Источник данных
                        t -> t.endsWith("4") // Условие
                );

        // Логирование в консоль и log-файл в папке target/test-attachments
        Logger.info("Получено значение " + timestamp);

        // Проверка
        Assert.isTrue(timestamp != null && timestamp.endsWith("4"), // Условие проверки
                "Полученное значение заканчивается на 4", // Описание проверки
                "Полученное значение не заканчивается на 4" // Сообщение об ошибке
        );

        var result = Wait
                .byDuration(
                        // Время на ожидание
                        Duration.ofSeconds(10),
                        // Интервал, через который повторяется проверка условия
                        Duration.ofMillis(200)
                )
                // Описание ожидания
                .description("Timestamp заканчивается на 4")
                // Нужно ли завершить тест и отметить его непройденным, если условие не выполнено;
                // сообщение об ошибке
                .needFailWithMessage("Не удалось получить нужное значение")
                // Метод ожидания
                .waitFor(
                        () -> ContentHelper.getTimestamp().endsWith("4") // Условие
                );

        Logger.info("Получено значение " + result);

        Assert.isTrue(result, "Полученное значение заканчивается на 4",
                "Полученное значение не заканчивается на 4");
    }

    @SuppressWarnings("ConstantConditions")
    @DisplayName("Примеры использования методов Assert")
    @Test
    void testAssertExamples() {
        var condition = true;
        Assert.isTrueAndAccumulate(condition, "condition равно true. Метод isTrueAndAccumulate",
                "Сообщение об ошибке 1");

        Assert.isTrueAndAccumulate(() -> condition, "condition равно true. Метод isTrueAndAccumulate",
                "Сообщение об ошибке 2");

        Assert.isGreaterAndAccumulate(10, 5, "Метод isGreaterAndAccumulate",
                "Сообщение об ошибке 3");

        Assert.isGreaterOrEqualAndAccumulate(15, 5, "Метод isGreaterOrEqualAndAccumulate",

                "Сообщение об ошибке 4");
        Assert.isGreaterOrEqualAndAccumulate(20, 20, "Метод isGreaterOrEqualAndAccumulate",
                "Сообщение об ошибке 5");

        Assert.areEqualAndAccumulate(25, 25, "Метод areEqualAndAccumulate",
                "Сообщение об ошибке 6");
        Assert.areEqualAndAccumulate("Пример", "Пример", "Метод areEqualAndAccumulate",
                "Сообщение об ошибке 6");

        Assert.areEqualByCompareAndAccumulate(BigDecimal.ONE, BigDecimal.valueOf(1.0),
                "Метод areEqualByCompareAndAccumulate", "Сообщение об ошибке 7");

        Assert.areNotEqualAndAccumulate(2, 4, "Метод areNotEqualAndAccumulate",
                "Сообщение об ошибке");
        Assert.areNotEqualAndAccumulate("Пример1", "Пример2", "Метод areNotEqualAndAccumulate",
                "Сообщение об ошибке");

        Assert.areNotEqualByCompareAndAccumulate(BigDecimal.ONE, BigDecimal.valueOf(10),
                "Метод areNotEqualByCompareAndAccumulate", "Сообщение об ошибке");

        Assert.containsAndAccumulate("Пример 1", "Текст Пример 1 текст", "Метод containsAndAccumulate",
                "Сообщение об ошибке");

        Assert.notContainsAndAccumulate("Пример 1", "Текст Пример 2 текст", "Метод notContainsAndAccumulate",
                "Сообщение об ошибке");

        Assert.containsAndAccumulate(1, List.of(1, 2, 3), "Метод containsAndAccumulate",
                "Сообщение об ошибке");

        Assert.notContainsAndAccumulate("qwe", Arrays.asList("wer", "ert"), "Метод notContainsAndAccumulate",
                "Сообщение об ошибке");

        String str = null;
        Assert.isNullAndAccumulate(str, "Метод isNullAndAccumulate", "Сообщение об ошибке");

        str = "Строка";
        Assert.isNotNullAndAccumulate(str, "Метод isNotNullAndAccumulate", "Сообщение об ошибке");
        Assert.isNotNullAndNotEmptyAndAccumulate(str, "Метод isNotNullAndNotEmptyAndAccumulate",
                "Сообщение об ошибке");
        var arr = new Integer[]{1, 3};
        Assert.isNotNullAndNotEmptyAndAccumulate(arr, "Метод isNotNullAndNotEmptyAndAccumulate",
                "Сообщение об ошибке");
    }

    private void deleteEmployee(Employees employee) {
        Assert.areEqualAndAccumulate(1, database.deleteEmployee(employee),
                "Количество строк, затронутое удалением из БД",
                "Неправильное количество строк, затронутое удалением из БД");
    }
}
