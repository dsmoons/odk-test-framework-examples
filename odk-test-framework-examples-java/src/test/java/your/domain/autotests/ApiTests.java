package your.domain.autotests;

import io.bitbucket.dsmoons.framework.odk.Assert;
import io.bitbucket.dsmoons.framework.odk.ContentHelper;
import io.bitbucket.dsmoons.framework.odk.Logger;
import io.bitbucket.dsmoons.framework.odk.api.RestException;
import io.bitbucket.dsmoons.framework.odk.api.RestResponse;
import io.qameta.allure.Epic;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import your.domain.autotests.services.rest.petstore.Order;
import your.domain.autotests.services.rest.petstore.Pet;

import java.math.BigInteger;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Random;

@SuppressWarnings("unused")
@DisplayName("Примеры тестов по api")
@Epic("Примеры тестов по api")
class ApiTests extends SuiteBaseApi {

    @DisplayName("[REST] Проверка методов сервиса Petstore")
    @Tag("Позитивный")
    @Test
    void checkPetstoreRestService() {
        var pets = restPetstoreClient.findByStatus(Pet.StatusEnum.AVAILABLE).getResponseObject();
        Assert.isNotNullAndNotEmpty(pets, "Ответ от сервиса", "Ответ от сервиса равен null или пустой");

        var pet = pets[(new Random()).nextInt(pets.length)];
        Logger.info("Выбран объект" + System.lineSeparator() + ContentHelper.objectToString(pet));

        var orderRequest = new Order();
        orderRequest.setPetId(pet.getId());
        orderRequest.setQuantity(1);
        orderRequest.setId(0L);
        orderRequest.setShipDate(OffsetDateTime.now());
        orderRequest.setComplete(true);
        orderRequest.setStatus(Order.StatusEnum.PLACED);
        var orderResponse = restPetstoreClient.customOrder(orderRequest);
        Assert.isNotNull(orderResponse.getResponseObject(), "Order", "Order равен null");

        Assert.areEqualAndAccumulate(200, orderResponse.getStatusCode(),
                "Код состояния HTTP в ответе от сервиса", "Неправильный код состояния HTTP");

        Assert.isGreaterAndAccumulate(orderResponse.getResponseObject().getId(), 0L,
                "Order.Id", "Order.Id равно 0");
        Assert.areEqualAndAccumulate(orderRequest.getPetId(), orderResponse.getResponseObject().getPetId(),
                "Order.PetId в запросе и ответе", "Не совпадает Order.PetId в запросе и ответе");
        Assert.areEqualAndAccumulate(orderRequest.getQuantity(), orderResponse.getResponseObject().getQuantity(),
                "Order.Quantity в запросе и ответе", "Не совпадает Order.Quantity в запросе и ответе");
        Assert.areEqualAndAccumulate(orderRequest.isComplete(), orderResponse.getResponseObject().isComplete(),
                "Order.Complete в запросе и ответе", "Не совпадает Order.Complete в запросе и ответе");
    }

    @DisplayName("[REST] Проверка ошибки метода order сервиса Petstore")
    @Tag("Негативный")
    @Test
    void checkPetstoreRestServiceOrderError() {
        // Объект запроса с заведомо ложными данными
        var orderRequest = new Order();
        orderRequest.setShipDate(OffsetDateTime.now().minusYears(999999998L));

        RestResponse<Order> orderResponse = null;
        try {
            orderResponse = restPetstoreClient.order(orderRequest, true);
        } catch (RestException restException) {
            Assert.areEqualAndAccumulate(500, restException.getHttpResponse().getCode(),
                    "Код состояния HTTP в ответе от сервиса", "Неправильный код состояния HTTP");
            Assert.containsAndAccumulate("something bad happened", restException.getFullMessage(),
                    "Сообщение об ошибке", "Неправильное сообщение об ошибке");

            Logger.info("Заголовки ответа:" + System.lineSeparator() +
                    Arrays.toString(restException.getHttpResponse().getHeaders()));
            Logger.info("Строка состояния: " + restException.getHttpResponse().getReasonPhrase());
        }
        Assert.isNull(orderResponse, "Order", "Есть ответ от сервиса, хотя ожидалось исключение");
    }

    @DisplayName("[SOAP] Проверка метода numberToWords сервиса NumberConversion")
    @Tag("Позитивный")
    @Test
    void numberToWordsNumberConversionServiceTest() {
        var words = soapNumberConversionServiceClient.numberToWords(BigInteger.valueOf(42L)).getResponseObject();
        Assert.areEqual("forty two ", words, "Неправильный ответ от сервиса");
    }
}
