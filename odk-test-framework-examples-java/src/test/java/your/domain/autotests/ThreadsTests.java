package your.domain.autotests;

import io.bitbucket.dsmoons.framework.odk.AllureHelper;
import io.bitbucket.dsmoons.framework.odk.Assert;
import io.bitbucket.dsmoons.framework.odk.Logger;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Execution(ExecutionMode.SAME_THREAD)
@DisplayName("Примеры тестов с использованием потоков")
public class ThreadsTests extends SuiteBaseApi {

    @DisplayName("Тест с использованием ExecutorService")
    @Test
    void executorServiceTest() {
        var endNumber = 5;
        var executorService = Executors.newCachedThreadPool();
        for (var i = 0; i < endNumber; i++) {
            int finalI = i;
            executorService.submit(() -> check(finalI, endNumber));
        }
        executorService.shutdown();
        try {
            var count = 0;
            while (!executorService.awaitTermination(100, TimeUnit.MILLISECONDS)) {
                Logger.info("Ожидание " + count);
                count++;
            }
        } catch (Exception exception) {
            Assert.fail(exception);
        }
    }

    @DisplayName("Тест с использованием parallelStream")
    @Test
    void parallelStreamTest() {
        var endNumber = 5;
        IntStream.range(1, endNumber).boxed().toList().parallelStream().forEach(i -> check(i, endNumber));
    }

    private void check(int i, int number) {
        threadUtil.createObjects(); // Первым действием в новом потоке должен быть вызван этот метод
        var message = String.format("%s в потоке %s с id %s", i,
                Thread.currentThread().getName(), Thread.currentThread().getId());
        AllureHelper.runStep("Шаг номер " + message, () -> {
            Logger.info("Число " + message);
            Assert.isGreaterAndAccumulate(number, i, "Число " + i + " меньше " + number,
                    "Число " + i + " больше " + number);
        });
    }
}
