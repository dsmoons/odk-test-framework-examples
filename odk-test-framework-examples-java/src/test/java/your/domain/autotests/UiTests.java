package your.domain.autotests;

import io.bitbucket.dsmoons.framework.odk.Assert;
import io.bitbucket.dsmoons.framework.odk.ImageHelper;
import io.bitbucket.dsmoons.framework.odk.Logger;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.UUID;

@SuppressWarnings("CommentedOutCode")
@DisplayName("Браузерные тесты")
@Epic("Браузерные тесты")
class UiTests extends SuiteBaseUi {

    @DisplayName("Проверка первого поста")
    @Test
    void checkFirstPost() {
        pages.hqDefault.open();
        pages.hqDefault.firstPost.compareHeaderAndNextLink();

        var firstPostHeaderOnMainPage = pages.hqDefault.firstPost.getHeaderText();
        var firstPostParagraphTextOnMainPage = pages.hqDefault.firstPost.getFirstParagraphText();
        var firstPostDateOnMainPage = pages.hqDefault.firstPost.getDate();
        var firstPostTagsOnMainPage = pages.hqDefault.firstPost.getTags();
        pages.hqDefault.firstPost.goToPostPage();

        var headerOnPostPage = pages.post.getPostHeaderText();
        Assert.areEqualAndAccumulate(firstPostHeaderOnMainPage, headerOnPostPage,
                "Заголовок поста", "Не совпадают заголовки");

        var firstParagraphTextOnPostPage = pages.post.getFirstParagraphText();
        Assert.areEqualAndAccumulate(firstPostParagraphTextOnMainPage, firstParagraphTextOnPostPage,
                "Текст первого параграфа первого поста",
                "Не совпадает текст первого параграфа первого поста");

        var postDate = pages.post.getDate();
        Assert.areEqualAndAccumulate(firstPostDateOnMainPage, postDate,
                "Дата первого поста", "Не совпадает дата первого поста");

        var postTags = pages.post.getTags();
        Assert.areEqual(firstPostTagsOnMainPage, postTags,
                "Теги первого поста", "Не совпадают теги первого поста");
    }

    @DisplayName("Действия с вкладками")
    @Test
    void testOperationsWithTabs() {
        pages.hqDefault.open()
                .getWindow().openNewTab()
                .and().open()
                .getWindow().savePageAsPdfFile().openNewTab().savePageAsPdfFile().openNewTab()
                .and()
                .open().getWindow().openNewTab()
                .switchToFirstTab().switchToNextTab()
                .switchToNextTab()
                .switchToPreviousTab()
                .switchToPreviousTab()
                .switchToLastTab()
                .and()
                .sleep(Duration.ofMillis(1000))
                .getWindow()
                .closeTabAndSwitchToLast()
                .closeTabAndSwitchToLast()
                .closeTabAndSwitchToLast()
                .closeTabAndSwitchToFirst()
                .and()
                .open().firstPost.compareHeaderAndNextLink();
    }

    @DisplayName("Действия с окном браузера")
    @Test
    void testOperationsWithBrowserWindow() {
        pages.hqDefault.open()
                .getWindow().setSize(1000, 600)
                .and()
                .sleep(Duration.ofMillis(1000))
                .getWindow().setFullscreen()
                .and()
                .sleep(Duration.ofMillis(1000))
                .getWindow().minimize()
                .and()
                .sleep(Duration.ofMillis(1000))
                .getWindow().maximize()
                .and()
                .firstPost.compareHeaderAndNextLink();
    }

    @DisplayName("Действия с элементами")
    @Test
    void testOperationsWithElements() {
        pages.hqDefault.open()
                .firstPostReadLinkActions()
                .firstPostLinkActions();
    }

    @DisplayName("Действия с двумя браузерами")
    @Test
    void testOperationsWithTwoBrowsers() {
        var halfWidth = java.awt.Toolkit.getDefaultToolkit().getScreenSize().width / 2;
        var height = java.awt.Toolkit.getDefaultToolkit().getScreenSize().height;

        pages.hqDefault.open()
                .getWindow()
                .setSize(halfWidth, height)
                .setPosition(halfWidth + 1, 0);

        pagesClone.hqDefault.open()
                .getWindow()
                .setSize(halfWidth, height)
                .setPosition(0, 0)
                .and()
                .firstPost
                .compareHeaderAndNextLink()
                .goToPostPage();

        pages.hqDefault.firstPost
                .compareHeaderAndNextLink()
                .goToPostPage();

        Logger.info(pagesClone.post.getPostHeaderText());
    }

    @DisplayName("Тест сравнения верстки")
    @Description("Создание скриншотов и их сравнение с помощью ImageHelper")
    @Test
    void testScreenshotsCompare() {
        var contactsPage = pages.hqDefault.open().topMenu.goToContactsPage();
        var elementScreenshot1 = contactsPage.setName(UUID.randomUUID().toString()).getNameScreenshot();
        var pageScreenshot1 = contactsPage.getWindow().takeScreenshot();
        contactsPage.refresh();
        var elementScreenshot2 = contactsPage.setName(UUID.randomUUID().toString()).getNameScreenshot();
        var pageScreenshot2 = contactsPage.getWindow().takeScreenshot();
        ImageHelper.compareScreenshots(elementScreenshot1, elementScreenshot2, "Элемент с разными данными");
        ImageHelper.compareScreenshots(pageScreenshot1, pageScreenshot2, "Страница после обновления");
        ImageHelper.compareScreenshots(pageScreenshot1, elementScreenshot2, "Страница после обновления");
        ImageHelper.compareScreenshots(elementScreenshot2, pageScreenshot1, "Страница после обновления");
    }

    @DisplayName("Выполнение javascript и взаимодействие с диалоговыми окнами")
    @Test
    void checkJavascriptAndAlerts() {
        pages.hqDefault.open();

        // Выполнение скрипта
        var userAgent = pages.hqDefault.getJavaScriptExecutor().executeScript("return window.navigator.userAgent");
        Assert.isNotNull(userAgent, "userAgent", "userAgent равно null");

        // Прикрепление скрипта для его вызова по id с разными аргументами
        var scriptId = pages.hqDefault.getJavaScriptExecutor().pinScript("alert(arguments[0])");

        // Выполнение скрипта по его id
        pages.hqDefault.getJavaScriptExecutor().executeScript(scriptId, "argument 1");
        pages.hqDefault.sleep(Duration.ofMillis(500));

        // Подтверждение алерта
        pages.hqDefault.getAlert().accept();

        // Выполнение скрипта по его id с другими аргументами
        pages.hqDefault.getJavaScriptExecutor().executeScript(scriptId, "argument 2", 45, "");
        pages.hqDefault.sleep(Duration.ofMillis(500)).getAlert().accept();

        // Открепление скрипта
        pages.hqDefault.getJavaScriptExecutor().unpinScript(scriptId);

        var text = "argument";
        pages.hqDefault.getJavaScriptExecutor().executeScript("prompt(arguments[0])", text);
        pages.hqDefault.sleep(Duration.ofMillis(500));

        // Получение текста из диалогового окна
        var alertText = pages.hqDefault.getAlert().getText();
        Assert.areEqualAndAccumulate(text, alertText,
                "Текст диалогового окна", "Неправильный текст диалогового окна");

        // Сообщение в диалоговое окно
        pages.hqDefault.getAlert().sendKeys("Сообщение");

        // Отклонение диалогового окна
        pages.hqDefault.getAlert().dismiss();

        pages.hqDefault.getJavaScriptExecutor().executeScript("confirm('Вопросы?')");
        pages.hqDefault.getAlert().dismiss();
        pages.hqDefault.topMenu.goToContactsPage();
    }

    /*@DisplayName("Перемещение браузера")
    @Test
    void testBrowserMovement() {
        var screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        var width = screenSize.width - 44;
        var height = screenSize.height - 67;
        var x = 1;
        var y = 1;
        var plusX = 1;
        var plusY = 1;
        pages.ya.open();
        pages.ya.getWindow().setSize(width, height);

        for (var i = 1; i <= 500; i++) {
            pages.ya.getWindow().setPosition(x, y);
            if ((x + width >= screenSize.width) || x <= 0) {
                plusX = -plusX;
            }
            if ((y + height >= screenSize.height) || y <= 0) {
                plusY = -plusY;
            }
            x += plusX;
            y += plusY;
        }

        pages.ya.getWindow().minimize();
    }*/

    /*@DisplayName("Изменение масштаба страницы")
    @Test
    void changeZoom() {
        pages.hqDefault.open();
        for (var i = 5; i <= 510; i += 5) {
            pages.hqDefault.getWindow().zoom(i);
            pages.hqDefault.sleep(50);
        }
    }*/
}
