package your.domain;

import io.bitbucket.dsmoons.framework.odk.ODkTestLauncher;

/**
 * <p>Класс с методом запуска автотестов из скомпилированного jar-файла. Не используйте его для других целей.</p>
 * <p>Необходимые для запуска тесты передаются в качестве параметров командной строки.</p>
 * <p>Примеры:</p>
 * <p>{@code > java -jar tests.jar class=your.domain.autotests.ApiTests}</p>
 * <p>{@code > java -jar tests.jar class=your.domain.autotests.ApiTests class=your.domain.autotests.HelpersExamplesTests}</p>
 * <p>{@code > java -jar tests.jar class=your.domain.autotests.ApiTests test=your.domain.autotests.UiTests#checkFirstPost}</p>
 * <p>См. метод {@link ODkTestLauncher#run(String[])}</p>
 */
public class TestLauncher {

    /*public static void main(String[] args) {
        ODkTestLauncher.run(args);
    }*/
}
