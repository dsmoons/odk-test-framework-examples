package your.domain.autotests

import io.bitbucket.dsmoons.framework.odk.DbHelper
import io.bitbucket.dsmoons.odk.sql.query.builder.PredicateObject.field
import io.bitbucket.dsmoons.odk.sql.query.builder.QueryObject.delete
import io.bitbucket.dsmoons.odk.sql.query.builder.QueryObject.select
import your.domain.autotests.db.chinook.Employees

class Database {

    //private val db = DbHelper("jdbcUrl").setDriverClass(org.sqlite.JDBC.class)
    //private val db = DbHelper("jdbcUrl").setDriverClass(org.sqlite.JDBC.class).setCountOfEntitiesForLogging(200)
    //private val db = DbHelper("jdbcUrl", "login", "password")
    //private val db = DbHelper("jdbcUrl", "login", "password").setDriverClass(org.sqlite.JDBC.class)
    //private val db = DbHelper("jdbcUrl", "login", "password").setDriverClass(org.sqlite.JDBC.class).setCountOfEntitiesForLogging(200)
    private val sqlite =
        DbHelper("jdbc:sqlite:src/main/resources/db/chinook.db") //.setCatalog("catalogName").setSchema("schemaName")

    fun getEmployeesCount(): Int = select("count(*)").from(Employees::class).build().let {
        sqlite.executeQuery.forListAndGetFirst(it, Int::class, "Получение количества записей в таблице employees")
    } as Int

    fun getEmployeesListByCity(city: String): List<Employees> =
        select().from(Employees::class).where(field(Employees::city).equalTo(city)).build().let {
            val message = "Получение записей из таблицы employees по названию города"
            sqlite.executeQuery.forList(it, Employees::class, message)
        }

    fun getEmployeeByName(firstName: String, lastName: String): Employees =
        select().from(Employees::class)
            .where(field(Employees::firstName).equalTo(firstName).and().field(Employees::lastName).equalTo(lastName))
            .build().let {
                val message = "Получение записи из таблицы employees по имени"
                sqlite.executeQuery.forListAndGetFirst(it, Employees::class, message)
            } as Employees

    fun deleteEmployee(employee: Employees): Int =
        delete().from(Employees::class).where(field(Employees::firstName).equalTo(employee.firstName)).build()
            .let { sqlite.executeQuery.withoutResponse(it, "Удаление записи с FirstName = ${employee.firstName}") }

    fun insertEmployee(employee: Employees): Int = sqlite.insert.entity("employees", employee, "Новый employee")

    fun insertEmployees(employees: List<Employees>): Int =
        sqlite.insert.entities("employees", employees, "Новые employee")
}
