@file:Suppress("unused")

package your.domain.autotests.pages.hq.custom.elements

import io.bitbucket.dsmoons.framework.odk.AllureHelper
import io.bitbucket.dsmoons.framework.odk.BaseHelper
import io.bitbucket.dsmoons.framework.odk.PageBase
import your.domain.autotests.pages.hq.Contacts

class TopMenu(baseHelper: BaseHelper) : PageBase<TopMenu>(baseHelper) {
    private val menu = getElementById("top-menu", "Верхнее меню")
    private val blog = getElementByXpath(menu, "//*[text()='Блог']", "Ссылка на блог")
    private val odk = getElementByXpath(menu, "//*[text()='ODk Test Framework']", "Ссылка 'ODk Test Framework'")
    private val odf = getElementByXpath(menu, "//*[text()='ODf Test Framework']", "Ссылка 'ODf Test Framework'")
    private val contacts = getElementByXpath(menu, "//*[text()='Контакты']", "Ссылка на контакты")

    fun goToContactsPage(): Contacts = AllureHelper.runStep<Contacts>("Переход на страницу контактов") {
        contacts.click().waitFor({ it.notExists() })
        getPage(Contacts::class.java)
    } as Contacts
}
