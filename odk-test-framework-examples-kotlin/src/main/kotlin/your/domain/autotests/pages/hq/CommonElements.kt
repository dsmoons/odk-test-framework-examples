@file:Suppress("unused")

package your.domain.autotests.pages.hq

import io.bitbucket.dsmoons.framework.odk.AllureHelper
import io.bitbucket.dsmoons.framework.odk.BaseHelper
import io.bitbucket.dsmoons.framework.odk.PageBase
import io.bitbucket.dsmoons.framework.odk.wrappers.Element
import your.domain.autotests.pages.hq.custom.elements.TopMenu

open class CommonElements<T : PageBase<T>>(base: BaseHelper, link: String? = null) : PageBase<T>(base, link) {
    var topMenu = getPage(TopMenu::class.java)

    // Примеры классов, облегающих создание элементов
    inner class Button(text: String) : Element(getElementByXpath("//button[text='$text']", "Кнопка '$text'"))

    inner class Checkbox(label: String) :
        Element(getElementByXpath("//label[text='$label']/input[@type='checkbox']", "Флажок '$label'")) {

        fun check(): Checkbox = apply {
            AllureHelper.runStep("Отметить флажок, если он не отмечен") { takeUnless { getSelect().isSelected() }?.click() }
        }

        fun uncheck(): Checkbox = apply {
            AllureHelper.runStep("Снять отметку с флажка, если он отмечен") { takeIf { getSelect().isSelected() }?.click() }
        }
    }
}
