@file:Suppress("unused")

package your.domain.autotests.pages

import io.bitbucket.dsmoons.framework.odk.BaseHelper
import io.bitbucket.dsmoons.framework.odk.PageHelper
import your.domain.autotests.pages.hq.Contacts
import your.domain.autotests.pages.hq.Default
import your.domain.autotests.pages.hq.Post

class Pages(baseHelper: BaseHelper) : PageHelper(baseHelper) {

    val contacts = getPage(Contacts::class.java)
    val default = getPage(Default::class.java)
    val post = getPage(Post::class.java)
}
