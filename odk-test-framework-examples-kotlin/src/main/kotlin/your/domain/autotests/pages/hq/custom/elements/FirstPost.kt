package your.domain.autotests.pages.hq.custom.elements

import io.bitbucket.dsmoons.framework.odk.AllureHelper
import io.bitbucket.dsmoons.framework.odk.Assert
import io.bitbucket.dsmoons.framework.odk.BaseHelper
import io.bitbucket.dsmoons.framework.odk.PageBase

open class FirstPost(baseHelper: BaseHelper) : PageBase<FirstPost>(baseHelper) {

    private val post = getElementByXpath("//div[@class='post'][1]", "Первый пост")
    private val header = getElementByXpath(post, "//h2/a", "Заголовок-ссылка")
    private val firstParagraph = getElementByXpath(post, "//p", "Первый параграф")
    private val date = getElementByClassName(post, "data", "Дата")
    private val next = getElementByXpath(post, "//span[@class='next']/a", "Ссылка 'Читать далее'")
    private val tags = getElementByClassName(post, "tag", "Теги")

    fun compareHeaderAndNextLink(): FirstPost = apply {
        AllureHelper.runStep("Сравнение ссылок на пост") {
            val headerLink = header.waitForExists().getDomAttribute("href")
            val nextLink = next.getDomAttribute("href")
            Assert.areEqualAndAccumulate(headerLink, nextLink, "Ссылки на пост", "Не совпадают ссылки на пост")
        }
    }

    fun getFirstParagraphText(): String? =
        AllureHelper.runStep<String>("Получение текста первого параграфа первого поста на главной странице") {
            firstParagraph.getText()
        }

    fun getDate(): String? =
        AllureHelper.runStep<String>("Получение даты первого поста на главной странице") { date.getText() }

    fun getTags(): String? =
        AllureHelper.runStep<String>("Получение тегов первого поста на главной странице") { tags.getText() }

    fun getHeaderText(): String? =
        AllureHelper.runStep<String>("Получение текста ссылки-заголовка первого поста на главной странице") {
            header.getText()
        }

    fun goToPostPage() =
        AllureHelper.runStep("Переход на страницу первого поста") { next.click().waitFor({ it.notExists() }) }
}
