package your.domain.autotests.pages.hq

import io.bitbucket.dsmoons.framework.odk.AllureHelper
import io.bitbucket.dsmoons.framework.odk.BaseHelper
import io.bitbucket.dsmoons.framework.odk.PropertiesHelper
import io.bitbucket.dsmoons.framework.odk.wrappers.Element
import your.domain.autotests.pages.hq.custom.elements.FirstPost

class Default(base: BaseHelper) : CommonElements<Default>(base, PropertiesHelper.getProperty("link.hq")) {

    val firstPost = getPage(FirstPost::class.java)

    private val firstPostLink = getElementByXpath("//div/h2/a", "Ссылка на пост")
    private val firstPostReadLink = getElementByXpath("//span[@class='next']/a", "Ссылка 'Читать далее'")

    fun firstPostReadLinkActions(): Default = apply {
        AllureHelper.runStep("Действия с элементом $firstPostReadLink") {
            firstPostReadLink
                .waitForExistsAnd({ it.getText() != "" })
                .assertThat(Element::exists, "Элемент не найден")
                .assertThat(Element::isEnabled, "Элемент недоступен")
                .takeScreenshot()
        }
    }

    fun firstPostLinkActions(): Default = apply {
        AllureHelper.runStep("Действия с элементом $firstPostLink") {
            firstPostLink
                .assertThat(Element::exists, "Элемент не найден")
                .toList()
                .forEach { it.takeScreenshot() }
        }
    }
}
