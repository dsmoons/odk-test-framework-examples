package your.domain.autotests.pages.hq

import io.bitbucket.dsmoons.framework.odk.AllureHelper
import io.bitbucket.dsmoons.framework.odk.BaseHelper

class Post(base: BaseHelper) : CommonElements<Post>(base) {

    private val postHeader = getElementByXpath("//div[@class='content']/h2", "Заголовок поста")
    private val firstParagraphText = getElementByClassName("first", "Первый параграф")
    private val date = getElementByClassName("data", "Дата")
    private val tags = getElementByClassName("tag", "Теги")

    fun getPostHeaderText(): String? =
        AllureHelper.runStep<String>("Получение заголовка поста") { postHeader.getText() }

    fun getFirstParagraphText(): String? =
        AllureHelper.runStep<String>("Получение текста первого параграфа") { firstParagraphText.getText() }

    fun getDate(): String? = AllureHelper.runStep<String>("Получение даты поста") { date.getText() }

    fun getTags(): String? = AllureHelper.runStep<String>("Получение тегов поста") { tags.getText() }
}
