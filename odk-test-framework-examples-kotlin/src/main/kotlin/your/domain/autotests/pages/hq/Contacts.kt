package your.domain.autotests.pages.hq

import io.bitbucket.dsmoons.framework.odk.AllureHelper
import io.bitbucket.dsmoons.framework.odk.BaseHelper
import io.bitbucket.dsmoons.framework.odk.wrappers.Element
import java.io.File

class Contacts(baseHelper: BaseHelper) : CommonElements<Contacts>(baseHelper) {

    private val nameElement = getElementById("full-name", "Поле 'Имя'")

    val nameScreenshot: File?
        get() = nameElement.takeScreenshot()

    fun setName(name: String): Contacts =
        apply { AllureHelper.runStep<Element>("Заполнение имени") { nameElement.waitForExists().sendKeys(name) } }
}