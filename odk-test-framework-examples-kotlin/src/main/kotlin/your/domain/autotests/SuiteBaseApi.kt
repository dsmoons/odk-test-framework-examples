package your.domain.autotests

import io.bitbucket.dsmoons.framework.odk.BaseHelper
import io.bitbucket.dsmoons.framework.odk.HandleException
import io.bitbucket.dsmoons.framework.odk.PropertiesHelper
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInfo
import org.junit.jupiter.api.parallel.Execution
import org.junit.jupiter.api.parallel.ExecutionMode
import your.domain.autotests.serviceClient.RestPetstoreClient
import your.domain.autotests.serviceClient.SoapNumberConversionServiceClient

@Suppress("MemberVisibilityCanBePrivate")
@HandleException
@Execution(ExecutionMode.CONCURRENT)
open class SuiteBaseApi {

    lateinit var soapNumberConversionServiceClient: SoapNumberConversionServiceClient
    lateinit var restPetstoreClient: RestPetstoreClient

    protected val database = Database()

    lateinit var threadUtil: BaseHelper.ThreadUtil

    private lateinit var base: BaseHelper

    @BeforeEach
    fun setUp(testInfo: TestInfo) {
        base = BaseHelper.Builder(testInfo)
            .setPropertiesFile("profile.properties")
            .setEnvironmentPropertiesFile("environment.properties")
            .build()

        threadUtil = base.getThreadUtil()

        soapNumberConversionServiceClient =
            SoapNumberConversionServiceClient(PropertiesHelper.getProperty("number.conversion.service.soap.wsdl.host"))
        restPetstoreClient = RestPetstoreClient(PropertiesHelper.getProperty("petstore.rest.host"))

        base.start()
    }

    @AfterEach
    fun tearDown() = base.stop()
}
