package your.domain.autotests.db.chinook

data class Employees(var employeeId: Int? = null,
                     var lastName: String? = null,
                     var firstName: String? = null,
                     var title: String? = null,
                     var reportsTo: Int? = null,
                     var birthDate: String? = null,
                     var hireDate: String? = null,
                     var address: String? = null,
                     var city: String? = null,
                     var state: String? = null,
                     var country: String? = null,
                     var postalCode: String? = null,
                     var phone: String? = null,
                     var fax: String? = null,
                     var email: String? = null)