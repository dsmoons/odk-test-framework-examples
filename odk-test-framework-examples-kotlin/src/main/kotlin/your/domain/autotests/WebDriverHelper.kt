package your.domain.autotests

import io.bitbucket.dsmoons.framework.odk.Assert
import io.bitbucket.dsmoons.framework.odk.PropertiesHelper
import io.github.bonigarcia.wdm.WebDriverManager
import org.openqa.selenium.MutableCapabilities
import org.openqa.selenium.PageLoadStrategy
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.edge.EdgeDriver
import org.openqa.selenium.edge.EdgeOptions
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.firefox.GeckoDriverService
import org.openqa.selenium.remote.LocalFileDetector
import org.openqa.selenium.remote.RemoteWebDriver
import java.io.File
import java.net.URL

internal object WebDriverHelper {

    fun getDriver(browser: String = PropertiesHelper.getProperty("browser")): WebDriver =
        when (browser) {
            "SelenoidChrome" -> getSelenoidChrome()
            "SelenoidFirefox" -> getSelenoidFirefox()
            "Chrome" -> getChromeDriver()
            "ChromeHeadless" -> getChromeDriver(true)
            "Firefox" -> getFirefoxDriver()
            "FirefoxHeadless" -> getFirefoxDriver(true)
            "Edge" -> getEdgeDriver(false)
            "EdgeHeadless" -> getEdgeDriver(true)
            else -> getChromeDriver()
        }

    private fun getChromeDriver(headless: Boolean = false): ChromeDriver {
        WebDriverManager.chromedriver().setup()
        return ChromeDriver(getChromeOptions(headless))
    }

    private fun getFirefoxDriver(headless: Boolean = false): FirefoxDriver {
        System.setProperty(GeckoDriverService.GECKO_DRIVER_LOG_PROPERTY, "/dev/null")
        WebDriverManager.firefoxdriver().setup()
        return FirefoxDriver(getFirefoxOptions(headless))
    }

    private fun getEdgeDriver(headless: Boolean): EdgeDriver {
        WebDriverManager.edgedriver().setup()
        val options = EdgeOptions().apply {
            setPageLoadStrategy(PageLoadStrategy.NORMAL)
            addArguments("--no-sandbox", "--remote-allow-origins=*", "--disable-gpu", "--disable-dev-shm-usafe")
            if (headless) {
                addArguments("--headless")
            }
        }
        return EdgeDriver(options)
    }

    private fun getSelenoidChrome(): RemoteWebDriver = getSelenoidRemoteWebDriver(getChromeOptions(false))

    private fun getSelenoidFirefox(): RemoteWebDriver = getSelenoidRemoteWebDriver(getFirefoxOptions(false))

    private fun getChromeOptions(headless: Boolean): ChromeOptions =
        ChromeOptions().apply {
            //setCapability("safebrowsing.enabled", "false")
            addArguments("--no-sandbox", "--remote-allow-origins=*", "--disable-gpu", "--disable-dev-shm-usafe")
            if (headless) {
                addArguments("--headless")
            }
            setAcceptInsecureCerts(true)
            val prefs =
                hashMapOf<String, Any>(Pair("download.default_directory", File("target//downloads").absolutePath))
            setExperimentalOption("prefs", prefs)
            setPageLoadStrategy(PageLoadStrategy.NORMAL)
        }

    private fun getFirefoxOptions(headless: Boolean): FirefoxOptions =
        FirefoxOptions().apply {
            val mimeTypes = "text/csv,application/msexcel,application/x-msexcel,application/excel," +
                    "application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain," +
                    "application/msword,application/xml,application/pdf,application/octet-stream"
            addPreference("browser.download.folderList", 2)
            addPreference("browser.download.manager.showWhenStarting", false)
            addPreference("browser.download.dir", File("target//downloads").absolutePath)
            addPreference("browser.helperApps.neverAsk.openFile", mimeTypes)
            addPreference("browser.helperApps.neverAsk.saveToDisk", mimeTypes)
            addPreference("browser.helperApps.alwaysAsk.force", false)
            addPreference("browser.download.manager.alertOnEXEOpen", false)
            addPreference("browser.download.manager.focusWhenStarting", false)
            addPreference("browser.download.manager.useWindow", false)
            addPreference("browser.download.manager.showAlertOnComplete", false)
            addPreference("browser.download.manager.closeWhenDone", false)
            addPreference("browser.startup.homepage_override.mstone", "ignore")
            addPreference("startup.homepage_welcome_url.additional", "about:blank")
            addPreference("plugin.scan.Acrobat", "99.0")
            addPreference("plugin.scan.plid.all", false)
            addPreference("plugin.disable_full_page_plugin_for_types", mimeTypes)
            addPreference("pdfjs.disabled", true)
            setPageLoadStrategy(PageLoadStrategy.NORMAL)
            if (headless) {
                addArguments("--headless")
            }
        }

    private fun getSelenoidRemoteWebDriver(capabilities: MutableCapabilities): RemoteWebDriver {
        capabilities.setCapability(
            "selenoid:options", mapOf(Pair("enableVNC", true), Pair("enableVideo", false), Pair("enableLog", false))
        )
        val url = try {
            URL(PropertiesHelper.getProperty("selenoid.host"))
        } catch (exception: Exception) {
            Assert.handleException(exception, "Ошибка создания ссылки на драйвер")
        }
        return RemoteWebDriver(url, capabilities).apply { fileDetector = LocalFileDetector() }
    }
}
