@file:Suppress("unused")

package your.domain.autotests.serviceClient

import io.bitbucket.dsmoons.framework.odk.ContentHelper.serializeToJsonString
import io.bitbucket.dsmoons.framework.odk.api.RestResponse
import io.bitbucket.dsmoons.framework.odk.api.RestServiceClientHelper
import jakarta.ws.rs.core.MediaType
import org.apache.hc.core5.http.io.entity.StringEntity
import org.apache.hc.core5.http.message.BasicClassicHttpRequest
import org.apache.hc.core5.http.message.BasicHeader
import your.domain.services.rest.petstore.Order
import your.domain.services.rest.petstore.Pet
import java.nio.charset.StandardCharsets

class RestPetstoreClient(host: String) :
    RestServiceClientHelper(host, BasicHeader("Content-Type", "${MediaType.APPLICATION_JSON}; charset=utf-8")) {

    fun findByStatus(status: Pet.StatusEnum, needThrow: Boolean = false): RestResponse<Array<Pet>> =
        sendGetRequest(needThrow, Array<Pet>::class.java, "v2/pet/findByStatus?status=${status}")

    fun order(request: Order, needThrow: Boolean = false): RestResponse<Order> =
        sendPostRequest(needThrow, Order::class.java, "v2/store/order", request)

    fun customOrder(request: Order): RestResponse<Order> = BasicClassicHttpRequest("POST", "")
        .apply { entity = StringEntity(request.serializeToJsonString(), StandardCharsets.UTF_8) }
        .let { sendCustomRequest(Order::class.java, "v2/store/order", it) }
}
