package your.domain.autotests.serviceClient

import com.dataaccess.webservicesserver.NumberConversion
import io.bitbucket.dsmoons.framework.odk.api.SoapResponse
import io.bitbucket.dsmoons.framework.odk.api.SoapServiceClientHelper
import java.math.BigInteger
import java.net.URL

class SoapNumberConversionServiceClient(serviceLink: String) :
    SoapServiceClientHelper({ NumberConversion(URL(serviceLink)).numberConversionSoap12 }) {

    fun numberToWords(number: BigInteger): SoapResponse<String> = getResponse("numberToWords", number)
}