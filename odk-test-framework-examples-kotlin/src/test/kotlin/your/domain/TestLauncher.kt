package your.domain

import io.bitbucket.dsmoons.framework.odk.ODkTestLauncher

/**
 * Метод запуска автотестов из скомпилированного jar-файла. Не используйте его для других целей.
 *
 * Необходимые для запуска тесты передаются в качестве параметров командной строки.
 *
 * Примеры:
 *
 * `> java -jar tests.jar class=your.domain.autotests.ApiTests`
 *
 * `> java -jar tests.jar class=your.domain.autotests.ApiTests class=your.domain.autotests.HelpersExamplesTests`
 *
 * `> java -jar tests.jar class=your.domain.autotests.ApiTests test=your.domain.autotests.UiTests#checkFirstPost`
 *
 * См. метод [ODkTestLauncher.run]
 */
//fun main(args: Array<String>) = ODkTestLauncher.run(args)
