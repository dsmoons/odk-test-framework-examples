package your.domain.autotests

import io.bitbucket.dsmoons.framework.odk.AllureHelper
import io.bitbucket.dsmoons.framework.odk.Assert
import io.bitbucket.dsmoons.framework.odk.Logger
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.parallel.Execution
import org.junit.jupiter.api.parallel.ExecutionMode
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

@Execution(ExecutionMode.SAME_THREAD)
@DisplayName("Примеры тестов с использованием потоков")
class ThreadsTests : SuiteBaseApi() {

    @DisplayName("Тест с использованием ExecutorService")
    @Test
    fun executorServiceTest() {
        val endNumber = 5
        val executorService = Executors.newCachedThreadPool()
        for (i in 0 until endNumber) {
            executorService.submit { check(i, endNumber) }
        }
        executorService.shutdown()
        try {
            var count = 0
            while (!executorService.awaitTermination(100, TimeUnit.MILLISECONDS)) {
                Logger.info("Ожидание $count")
                count++
            }
        } catch (exception: Exception) {
            Assert.fail(exception)
        }
    }

    @DisplayName("Тест с использованием parallelStream")
    @Test
    fun parallelStreamTest() {
        val endNumber = 5
        IntRange(1, endNumber).toList().parallelStream().forEach { i: Int -> check(i, endNumber + 1) }
    }

    private fun check(i: Int, number: Int) {
        threadUtil.createObjects()
        val thread = Thread.currentThread()
        val message = String.format("%s в потоке %s с id %s", i, thread.name, thread.id)
        AllureHelper.runStep("Шаг номер $message") {
            Logger.info("Число $message")
            Assert.isGreaterAndAccumulate(number, i, "Число $i меньше $number", "Число $i больше $number")
        }
    }
}
