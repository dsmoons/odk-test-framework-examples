@file:Suppress("KotlinConstantConditions")

package your.domain.autotests

import io.bitbucket.dsmoons.framework.odk.Assert
import io.bitbucket.dsmoons.framework.odk.ContentHelper
import io.bitbucket.dsmoons.framework.odk.Logger
import io.bitbucket.dsmoons.framework.odk.Wait
import io.qameta.allure.Epic
import org.jeasy.random.EasyRandom
import org.jeasy.random.EasyRandomParameters
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import your.domain.autotests.db.chinook.Employees
import java.math.BigDecimal
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

@DisplayName("Примеры тестов, показывающие работу хелперов")
@Epic("Примеры тестов, показывающие работу хелперов")
class HelpersExamplesTests : SuiteBaseApi() {

    @Test
    @DisplayName("Пример доступа к базе данных")
    fun testDatabase() {
        val count = database.getEmployeesCount()
        Assert.areEqualAndAccumulate(8, count, "Количество записей", "Неправильное количество записей")

        val employees = database.getEmployeesListByCity("Calgary")
        Assert.areEqualAndAccumulate(
            5, employees.size,
            "Количество записей по городу \"Calgary\"", "Неправильное количество записей"
        )

        val employee = database.getEmployeeByName("Robert", "King")
        Assert.areEqual("IT Staff", employee.title, "Title", "Неправильный Title")

        employee.apply {
            firstName += ContentHelper.getTimestamp()
            lastName += ContentHelper.getTimestamp()
            employeeId = null
        }
        val insertEmployee = database.insertEmployee(employee)
        Assert.areEqualAndAccumulate(
            1, insertEmployee, "Количество строк, затронутое вставкой в БД",
            "Неправильное количество строк, затронутое вставкой в БД"
        )

        deleteEmployee(employee)

        val newEmployees = EasyRandom(EasyRandomParameters().seed(ContentHelper.getTimestamp().toLong()))
            .objects(Employees::class.java, 2).toList().onEach { it.employeeId = null }

        val insertEmployees = database.insertEmployees(newEmployees)
        Assert.areEqualAndAccumulate(
            newEmployees.size, insertEmployees, "Количество строк, затронутое вставкой в БД",
            "Неправильное количество строк, затронутое вставкой в БД"
        )

        newEmployees.forEach(this::deleteEmployee)
    }

    @Test
    @DisplayName("Тест с ожиданием выполнения условия")
    fun testWait() {
        // Получение данных через ожидание выполнения условия
        val timestamp = Wait.byDuration(
            // Время на ожидание
            10.seconds,
            // Интервал, через который повторяется проверка условия
            500.milliseconds
        )
            // Описание ожидания
            .description("Timestamp заканчивается на 4")
            // Нужно ли завершить тест и отметить его непройденным, если условие не выполнено;
            // сообщение об ошибке
            .needFailWithMessage("Не удалось получить нужное значение")
            // Метод ожидания
            .waitFor(
                { ContentHelper.getTimestamp() }, // Источник данных
                { it.endsWith("4") } // Условие
            ) as String

        // Логирование в консоль и log-файл в папке target/test-attachments
        Logger.info("Получено значение $timestamp")

        // Проверка
        Assert.isTrue(
            timestamp.endsWith("4"), // Условие проверки
            "Полученное значение заканчивается на 4", // Описание проверки
            "Полученное значение не заканчивается на 4" // Сообщение об ошибке
        )

        // Ожидание выполнения условия
        val result = Wait.byDuration(
            // Время на ожидание
            10.seconds,
            // Интервал, через который повторяется проверка условия
            500.milliseconds
        )
            // Описание ожидания
            .description("Timestamp заканчивается на 4")
            // Нужно ли завершить тест и отметить его непройденным, если условие не выполнено;
            // сообщение об ошибке
            .needFailWithMessage("Не удалось получить нужное значение")
            // Метод ожидания
            .waitFor { ContentHelper.getTimestamp().endsWith("4") } // Условие

        // Логирование в консоль и log-файл в папке target/test-attachments
        Logger.info("Получено значение $result")

        // Проверка
        Assert.isTrue(
            result,
            "Полученное значение заканчивается на 4", // Описание проверки
            "Полученное значение не заканчивается на 4" // Сообщение об ошибке
        )
    }

    @Test
    @DisplayName("Примеры использования методов Assert")
    fun testAssertExamples() {
        val condition = true
        Assert.isTrueAndAccumulate(
            condition, "condition равно true. Метод isTrueAndAccumulate",
            "Сообщение об ошибке 1"
        )

        Assert.isTrueAndAccumulate(
            { condition }, "condition равно true. Метод isTrueAndAccumulate",
            "Сообщение об ошибке 2"
        )

        Assert.isGreaterAndAccumulate(10, 5, "Метод isGreaterAndAccumulate", "Сообщение об ошибке 3")

        Assert.isGreaterOrEqualAndAccumulate(15, 5, "Метод isGreaterOrEqualAndAccumulate", "Сообщение об ошибке 4")
        Assert.isGreaterOrEqualAndAccumulate(20, 20, "Метод isGreaterOrEqualAndAccumulate", "Сообщение об ошибке 5")

        Assert.areEqualAndAccumulate(25, 25, "Метод areEqualAndAccumulate", "Сообщение об ошибке 6")
        Assert.areEqualAndAccumulate("Пример", "Пример", "Метод areEqualAndAccumulate", "Сообщение об ошибке 7")

        Assert.areEqualByCompareAndAccumulate(
            BigDecimal.ONE, BigDecimal.valueOf(1.0),
            "Метод areEqualByCompareAndAccumulate", "Сообщение об ошибке 8"
        )

        Assert.areNotEqualAndAccumulate(2, 4, "Метод areNotEqualAndAccumulate", "Сообщение об ошибке 10")
        Assert.areNotEqualAndAccumulate(
            "Пример1", "Пример2",
            "Метод areNotEqualAndAccumulate", "Сообщение об ошибке 11"
        )

        Assert.areNotEqualByCompareAndAccumulate(
            BigDecimal.ONE, BigDecimal.valueOf(10),
            "Метод areNotEqualByCompareAndAccumulate", "Сообщение об ошибке 12"
        )

        Assert.containsAndAccumulate(
            "Пример 1", "Текст Пример 1 текст",
            "Метод containsAndAccumulate", "Сообщение об ошибке 13"
        )

        Assert.notContainsAndAccumulate(
            "Пример 1", "Текст Пример 2 текст",
            "Метод notContainsAndAccumulate", "Сообщение об ошибке 14"
        )

        Assert.containsAndAccumulate(1, listOf(1, 2, 3), "Метод containsAndAccumulate", "Сообщение об ошибке")

        Assert.notContainsAndAccumulate(
            "qwe", listOf("wer", "ert"),
            "Метод notContainsAndAccumulate", "Сообщение об ошибке"
        )

        var str: String? = null
        Assert.isNullAndAccumulate(str, "Метод isNullAndAccumulate", "Сообщение об ошибке 15")

        str = "Строка"
        Assert.isNotNullAndAccumulate(str, "Метод isNotNullAndAccumulate", "Сообщение об ошибке 16")
    }

    private fun deleteEmployee(employee: Employees) {
        val deleteEmployee = database.deleteEmployee(employee)
        Assert.areEqualAndAccumulate(
            1, deleteEmployee, "Количество строк, затронутое удалением из БД",
            "Неправильное количество строк, затронутое удалением из БД"
        )
    }
}
