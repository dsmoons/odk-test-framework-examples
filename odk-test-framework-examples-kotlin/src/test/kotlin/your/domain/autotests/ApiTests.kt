package your.domain.autotests

import io.bitbucket.dsmoons.framework.odk.Assert
import io.bitbucket.dsmoons.framework.odk.ContentHelper.getRandomElement
import io.bitbucket.dsmoons.framework.odk.ContentHelper.objectToString
import io.bitbucket.dsmoons.framework.odk.Logger
import io.bitbucket.dsmoons.framework.odk.api.RestException
import io.bitbucket.dsmoons.framework.odk.api.RestResponse
import io.qameta.allure.Epic
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import your.domain.services.rest.petstore.Order
import your.domain.services.rest.petstore.Pet
import java.math.BigInteger
import java.time.OffsetDateTime

@DisplayName("Тесты по api")
@Epic("Тесты по api")
internal class ApiTests : SuiteBaseApi() {

    @DisplayName("[REST] Проверка методов сервиса Petstore")
    @Tag("Позитивный")
    @Test
    fun checkPetstoreRestService() {
        val pets = restPetstoreClient.findByStatus(Pet.StatusEnum.AVAILABLE).responseObject
        Assert.isNotNullAndNotEmpty(pets, "Ответ от сервиса", "Ответ от сервиса равен null или пустой")

        val pet = pets.getRandomElement() as Pet
        Logger.info("Выбран объект${System.lineSeparator()}${pet.objectToString()}")

        val orderRequest = Order().apply {
            petId = pet.id
            quantity = 1
            id = 0L
            shipDate = OffsetDateTime.now()
            isComplete = true
            status = Order.StatusEnum.PLACED
        }
        val orderResponse = restPetstoreClient.order(orderRequest)
        Assert.isNotNull(orderResponse.responseObject, "Order", "Order равен null")
        Assert.areEqualAndAccumulate(
            200, orderResponse.statusCode,
            "Код состояния HTTP в ответе от сервиса", "Неправильный код состояния HTTP"
        )
        Assert.isGreaterAndAccumulate(orderResponse.responseObject.id, 0L, "Order.Id", "Order.Id равно 0")
        Assert.areEqualAndAccumulate(
            orderRequest.petId, orderResponse.responseObject.petId,
            "Order.PetId в запросе и ответе", "Не совпадает Order.PetId в запросе и ответе"
        )
        Assert.areEqualAndAccumulate(
            orderRequest.quantity, orderResponse.responseObject.quantity,
            "Order.Quantity в запросе и ответе", "Не совпадает Order.Quantity в запросе и ответе"
        )
        Assert.areEqualAndAccumulate(
            orderRequest.isComplete, orderResponse.responseObject.isComplete,
            "Order.Complete в запросе и ответе", "Не совпадает Order.Complete в запросе и ответе"
        )
    }

    @DisplayName("[REST] Проверка ошибки метода order сервиса Petstore")
    @Tag("Негативный")
    @Test
    fun checkPetstoreRestServiceOrderError() {
        // Объект запроса с заведомо ложными данными
        val orderRequest = Order().apply { shipDate = OffsetDateTime.now().minusYears(999999998L) }

        var orderResponse: RestResponse<Order>? = null
        try {
            // Запрос, который должен вызвать исключение
            orderResponse = restPetstoreClient.order(orderRequest, true)
        } catch (restException: RestException) {
            // Проверка содержимого исключения
            Assert.areEqualAndAccumulate(
                500, restException.httpResponse.code,
                "Код состояния HTTP в ответе от сервиса", "Неправильный код состояния HTTP"
            )
            Assert.containsAndAccumulate(
                "something bad happened", restException.fullMessage,
                "Сообщение об ошибке", "Неправильное сообщение об ошибке"
            )

            Logger.info(
                "Заголовки ответа:${System.lineSeparator()}" +
                        restException.httpResponse.headers.joinToString(System.lineSeparator())
            )
            Logger.info("Строка состояния: " + restException.httpResponse.reasonPhrase)
        }
        // Проверка того, что от сервиса не было ответа вместо исключения
        Assert.isNull(orderResponse, "Order", "Есть ответ от сервиса, хотя ожидалось исключение")
    }

    @DisplayName("[SOAP] Проверка метода numberToWords сервиса NumberConversion")
    @Tag("Позитивный")
    @Test
    fun numberToWordsNumberConversionServiceTest() {
        val words = soapNumberConversionServiceClient.numberToWords(BigInteger.valueOf(42L)).responseObject
        Assert.areEqual("forty two ", words, "Неправильный ответ от сервиса")
    }
}
