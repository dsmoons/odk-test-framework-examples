package your.domain.autotests

import io.bitbucket.dsmoons.framework.odk.Assert
import io.bitbucket.dsmoons.framework.odk.ImageHelper
import io.bitbucket.dsmoons.framework.odk.Logger
import io.qameta.allure.Description
import io.qameta.allure.Epic
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.awt.Toolkit
import java.util.*
import kotlin.time.Duration.Companion.milliseconds

@DisplayName("Браузерные тесты")
@Epic("Браузерные тесты")
internal class UiTests : SuiteBaseUi() {

    @DisplayName("Проверка первого поста")
    @Test
    fun checkFirstPost() {
        pages.default.open()
        pages.default.firstPost.compareHeaderAndNextLink()
        val firstPostHeaderOnMainPage = pages.default.firstPost.getHeaderText()
        val firstPostParagraphTextOnMainPage = pages.default.firstPost.getFirstParagraphText()
        val firstPostDateOnMainPage = pages.default.firstPost.getDate()
        val firstPostTagsOnMainPage = pages.default.firstPost.getTags()
        pages.default.firstPost.goToPostPage()
        val headerOnPostPage = pages.post.getPostHeaderText()
        Assert.areEqualAndAccumulate(
            firstPostHeaderOnMainPage, headerOnPostPage,
            "Заголовок поста", "Не совпадают заголовки"
        )
        val firstParagraphTextOnPostPage = pages.post.getFirstParagraphText()
        Assert.areEqualAndAccumulate(
            firstPostParagraphTextOnMainPage, firstParagraphTextOnPostPage,
            "Текст первого параграфа первого поста",
            "Не совпадает текст первого параграфа первого поста"
        )
        val postDate = pages.post.getDate()
        Assert.areEqualAndAccumulate(
            firstPostDateOnMainPage, postDate,
            "Дата первого поста", "Не совпадает дата первого поста"
        )
        val postTags = pages.post.getTags()
        Assert.areEqualAndAccumulate(
            firstPostTagsOnMainPage, postTags,
            "Теги первого поста", "Не совпадают теги первого поста"
        )
    }

    @DisplayName("Действия с вкладками")
    @Test
    fun testOperationsWithTabs() {
        pages.default.open()
            .getWindow().openNewTab()
            .and().open()
            .getWindow().savePageAsPdfFile().openNewTab().savePageAsPdfFile().openNewTab()
            .and()
            .open().getWindow().openNewTab()
            .switchToFirstTab().switchToNextTab()
            .switchToNextTab()
            .switchToPreviousTab()
            .switchToPreviousTab()
            .switchToLastTab()
            .and()
            .sleep(1000.milliseconds)
            .getWindow()
            .closeTabAndSwitchToLast()
            .closeTabAndSwitchToLast()
            .closeTabAndSwitchToLast()
            .closeTabAndSwitchToFirst()
            .and()
            .open().firstPost.compareHeaderAndNextLink()
    }

    @DisplayName("Действия с окном браузера")
    @Test
    fun testOperationsWithBrowserWindow() {
        pages.default.open()
            .getWindow().setSize(1000, 600)
            .and()
            .sleep(1000.milliseconds)
            .getWindow().setFullscreen()
            .and()
            .sleep(1000.milliseconds)
            .getWindow().minimize()
            .and()
            .sleep(1000.milliseconds)
            .getWindow().maximize()
            .and()
            .firstPost.compareHeaderAndNextLink()
    }

    @DisplayName("Действия с элементами")
    @Test
    fun testOperationsWithElements() {
        pages.default.open()
            .firstPostReadLinkActions()
            .firstPostLinkActions()
    }

    @DisplayName("Действия с двумя браузерами")
    @Test
    fun testOperationsWithTwoBrowsers() {
        val halfWidth = Toolkit.getDefaultToolkit().screenSize.width / 2
        val height = Toolkit.getDefaultToolkit().screenSize.height

        pages.default.open()
            .getWindow()
            .setSize(halfWidth, height)
            .setPosition(halfWidth + 1, 0)

        pagesClone.default.open()
            .getWindow()
            .setSize(halfWidth, height)
            .setPosition(0, 0)
            .and()
            .firstPost
            .compareHeaderAndNextLink()
            .goToPostPage()

        pages.default.firstPost
            .compareHeaderAndNextLink()
            .goToPostPage()

        Logger.info(pagesClone.post.getPostHeaderText())
    }

    @DisplayName("Тест сравнения верстки")
    @Description("Создание двух скриншотов и их сравнение с помощью ImageHelper")
    @Test
    fun testScreenshotsCompare() =
        with(pages.default.open().topMenu.goToContactsPage()) {
            val elementScreenshot1 = setName(UUID.randomUUID().toString()).nameScreenshot
            val pageScreenshot1 = getWindow().takeScreenshot()
            refresh()
            val elementScreenshot2 = setName(UUID.randomUUID().toString()).nameScreenshot
            val pageScreenshot2 = getWindow().takeScreenshot()
            ImageHelper.compareScreenshots(elementScreenshot1, elementScreenshot2, "Элемент с разными данными")
            ImageHelper.compareScreenshots(pageScreenshot1, pageScreenshot2, "Страница после обновления")
        }

    @DisplayName("Выполнение javascript и взаимодействие с диалоговыми окнами")
    @Test
    fun checkJavascriptAndAlerts() {
        pages.default.open()

        // Вылолнение скрипта
        val userAgent = pages.default.getJavaScriptExecutor()
            .executeScript<String>("return window.navigator.userAgent")
        Assert.isNotNullAndAccumulate(userAgent, "userAgent", "userAgent равно null")

        // Прикрепление скрипта для его вызова по id с разными аргументами
        val scriptId = pages.default.getJavaScriptExecutor().pinScript("alert(arguments[0])")

        // Выполнение скрипта по его id
        pages.default.getJavaScriptExecutor().executeScript<Unit>(scriptId, "argument 1")
        pages.default.sleep(500.milliseconds)

        // Подтверждение алерта
        pages.default.getAlert().accept()

        // Выполнение скрипта по его id с другими аргументами
        pages.default.getJavaScriptExecutor().executeScript<Unit>(scriptId, "argument 2", 45, "")
        pages.default.sleep(500.milliseconds)
        pages.default.getAlert().accept()

        // Открепление скрипта
        pages.default.getJavaScriptExecutor().unpinScript(scriptId)

        val text = "argument"
        pages.default.getJavaScriptExecutor().executeScript<Unit>("prompt(arguments[0])", text)
        pages.default.sleep(500.milliseconds)

        // Получение текста из диалогового окна
        val alertText = pages.default.getAlert().getText()
        Assert.areEqualAndAccumulate(
            text, alertText,
            "Текст диалогового окна", "Неправильный текст диалогового окна"
        )

        // Сообщение в диалоговое окно
        pages.default.getAlert().sendKeys("Сообщение")

        // Отклонение диалогового окна
        pages.default.getAlert().dismiss()

        pages.default.getJavaScriptExecutor().executeScript<Unit>("confirm('Вопросы?')")
        pages.default.getAlert().dismiss()
        pages.default.topMenu.goToContactsPage()
    }

    /*@DisplayName("Перемещение браузера")
    @Test
    fun testBrowserMovement() {
        pages.ya.open()
        val screenSize = java.awt.Toolkit.getDefaultToolkit().screenSize
        val width = screenSize.width - 44
        val height = screenSize.height - 67
        var x = 1
        var y = 1
        var plusX = 1
        var plusY = 1
        pages.ya.getWindow().setSize(width, height)

        for (i in 1..500) {
            pages.ya.getWindow().setPosition(x, y)
            if (x + width >= screenSize.width || x <= 0) {
                plusX = -plusX
            }
            if (y + height >= screenSize.height || y <= 0) {
                plusY = -plusY
            }
            x += plusX
            y += plusY
        }

        pages.ya.getWindow().minimize()
    }*/

    /*@DisplayName("Изменение масштаба страницы")
    @Test
    fun changeZoom() {
        pages.hqDefault.open()
        for (i in 5..510 step 5) {
            pages.hqDefault.getWindow().zoom(i)
            pages.hqDefault.sleep(50)
        }
    }*/
}
