# ODk Test Framework #

Примеры автотестов, написанных на Kotlin с помощью ODk Test Framework

**Первый запуск** (в Intellij Idea):

Открыть в Idea папку *odk-test-framework-examples-kotlin*

На вкладке Maven:  
1. Выбрать нужные профили __Browser__ и __Environment__  
2. Выполнить цель "__compile__". Дождаться "BUILD SUCCESS" в консоли

![Скриншот](readme1.png "Скриншот 1")

Для запуска одного теста кликнуть по кнопке "__Run Test__" слева от тестового метода.  
Для запуска всех тестов выполнить цели "__clean__", "__test__" на вкладке Maven

![Скриншот](readme2.png "Скриншот 2")

Для генерации отчета Allure после выполнения тестов выполнить цель "__allure:serve__" на вкладке Maven.  
При первом запуске в папку ".allure" будет загружен allure commandline.

![Скриншот](readme3.png "Скриншот 3")

